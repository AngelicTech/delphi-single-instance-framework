// ******************************************************************
//
// Program Name   : SIDemoFMX
// Platform(s)    : Android, iOS, OSX, Linux64, Win32, Win64
// Framework      : FMX
//
// Filename       : AT.SIDemo.FMX.Forms.Main.pas/.fmx
// File Version   : 1.00
// Date Created   : 08-Dec-2017
// Author         : Matthew Vesperman
//
// Description:
//
// Defines the demo's main form.
//
// Revision History:
//
// v1.00   :   Initial version
//
// ******************************************************************
//
// COPYRIGHT � 2017 - PRESENT Angelic Technology
// ALL RIGHTS RESERVED WORLDWIDE
//
// ******************************************************************

/// <summary>
///   Defines the demo's main form.
/// </summary>
unit AT.SIDemo.FMX.Forms.Main;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.Controls.Presentation, FMX.ScrollBox, FMX.Memo;

type
  TForm1 = class(TForm)
    Memo1: TMemo;
    procedure FormCreate(Sender: TObject);
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

uses
{$IF Defined(MSWINDOWS)}
  Winapi.Windows, FMX.Platform.Win,
{$ENDIF}
  AT.SingleInstance;

procedure TForm1.FormCreate(Sender: TObject);
begin
  //Set the form's caption based on the platform...
  {$IF Defined(ANDROID)}
    Self.Caption := 'Android Single Instance';
  {$ELSEIF Defined(IOS)}
    Self.Caption := 'iOS Single Instance';
  {$ELSEIF Defined(LINUX)}
    Self.Caption := 'Linux Single Instance';
  {$ELSEIF ( (Defined(MACOS)) AND (NOT Defined(IOS)) )}
    Self.Caption := 'MacOS Single Instance';
  {$ELSEIF Defined(WIN32)}
    Self.Caption := 'Windows 32bit Single Instance';
  {$ELSEIF Defined(WIN64)}
    Self.Caption := 'Windows 64bit Single Instance';
  {$ENDIF}

  Memo1.Lines.Clear;  //Empty memo...

  //Are we the first instance???
  if (SingleInstance.IsFirstInstance) then
    begin
      //Add message to memo indicating this is the first instance...
      Memo1.Lines.Add('This is the FIRST instance.');
      Memo1.Lines.Add(EmptyStr);

      //Add handler to the SingleInstance object using an anonymous
      //method, we don't care about the handler's guid so we ignore
      //it...
      SingleInstance.AddHandler(procedure(AParams: TStrings)
        begin
          Memo1.Lines.Add('Parameters Received');
          Memo1.Lines.Add('-------------------');
          Memo1.Lines.Add(AParams.Text);
          Memo1.Lines.Add('-------------------');
          Memo1.Lines.Add(EmptyStr);

          Self.Show;
          {$IF Defined(MSWINDOWS)}
          SetForegroundWindow(FmxHandleToHWND(Self.Handle));
          {$ENDIF}
        end);
    end;
end;

end.
