// ******************************************************************
//
// Program Name   : SIDemoVCL
// Platform(s)    : Win32, Win64
// Framework      : VCL
//
// Filename       : AT.SIDemo.VCL.Forms.Main.pas/.fmx
// File Version   : 1.00
// Date Created   : 08-Dec-2017
// Author         : Matthew Vesperman
//
// Description:
//
// Defines the demo's main form.
//
// Revision History:
//
// v1.00   :   Initial version
//
// ******************************************************************
//
// COPYRIGHT � 2017 - PRESENT Angelic Technology
// ALL RIGHTS RESERVED WORLDWIDE
//
// ******************************************************************

/// <summary>
///   Defines the demo's main form.
/// </summary>
unit AT.SIDemo.VCL.Forms.Main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.StdCtrls;

type
  TForm2 = class(TForm)
    Memo1: TMemo;
    procedure FormCreate(Sender: TObject);
  end;

var
  Form2: TForm2;

implementation

{$R *.dfm}

uses
  AT.SingleInstance;

procedure TForm2.FormCreate(Sender: TObject);
begin
  //Set the form's caption based on the platform...
  {$IF Defined(WIN32)}
    Self.Caption := 'Windows 32bit Single Instance';
  {$ELSEIF Defined(WIN64)}
    Self.Caption := 'Windows 64bit Single Instance';
  {$ENDIF}

  Memo1.Lines.Clear;  //Empty memo...

  //Are we the first instance???
  if (SingleInstance.IsFirstInstance) then
    begin
      //Add message to memo indicating this is the first instance...
      Memo1.Lines.Add('This is the FIRST instance.');
      Memo1.Lines.Add(EmptyStr);

      //Add handler to the SingleInstance object using an anonymous
      //method, we don't care about the handler's guid so we ignore
      //it...
      SingleInstance.AddHandler(procedure(AParams: TStrings)
        begin
          Memo1.Lines.Add('Parameters Received');
          Memo1.Lines.Add('-------------------');
          Memo1.Lines.Add(AParams.Text);
          Memo1.Lines.Add('-------------------');
          Memo1.Lines.Add(EmptyStr);

          Self.Show;
          SetForegroundWindow(Self.Handle);
        end);
    end;end;

end.
