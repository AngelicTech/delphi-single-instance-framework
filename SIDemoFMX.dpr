// ******************************************************************
//
// Program Name   : SIDemoFMX
// Platform(s)    : Android, iOS, OSX, Linux64, Win32, Win64
// Framework      : FMX
//
// Filename       : SIDemoFMX.dpr
// File Version   : 1.00
// Date Created   : 07-Dec-2017
// Author         : Matthew Vesperman
//
// Description:
//
// Sample FMX app to demonstrate the flexible, cross-platform single
// instance checking framework.
//
// Revision History:
//
// v1.00   :   Initial version
//
// ******************************************************************
//
// COPYRIGHT � 2017 - PRESENT Angelic Technology
// ALL RIGHTS RESERVED WORLDWIDE
//
// ******************************************************************

/// <summary>
///   Sample FMX app to demonstrate the flexible, cross-platform
///   single instance checking framework.
/// </summary>
program SIDemoFMX;

uses
  System.StartUpCopy,
  FMX.Forms,
  FMX.Dialogs,
  AT.SingleInstance.Base in 'Code Units\AT.SingleInstance.Base.pas',
  AT.SingleInstance in 'Code Units\AT.SingleInstance.pas',
  {$IF ( (Defined(MACOS)) AND (NOT Defined(IOS)) )}
  AT.SingleInstance.MacOS in 'Code Units\AT.SingleInstance.MacOS.pas',
  {$ELSEIF}
  AT.SingleInstance.Windows in 'Code Units\AT.SingleInstance.Windows.pas',
  {$ELSEIF}
  AT.SingleInstance.Linux in 'Code Units\AT.SingleInstance.Linux.pas',
  {$ENDIF }
  AT.SIDemo.FMX.Forms.Main in 'Forms\AT.SIDemo.FMX.Forms.Main.pas' {Form1};

{$R *.res}

var
  ASuccess: Boolean;  //Indicates if the check was successful...
  AIsFirst: Boolean;  //Flag to indicate first instance state...
  AError: Integer;    //Holds any error code...

begin
  //Check if we are the first instance...
  ASuccess := TATSingleInstance.CheckSingleInstance(
      '{66D1F0EB-7380-409D-AF04-A94E88828675}', AIsFirst, AError);

  //Was check successful???
  if (NOT ASuccess) then
    begin
      //No, display an error message...
      ShowMessageFmt('Single instance check failed! (Error: %d)',
          [AError]);

      //And terminate the app...
      Halt(AError);
    end;

  //Is this the first instance???
  if (AIsFirst) then
    begin
      //Yes, so start the listener to wait for param strings...
      SingleInstance.StartListening;

      //And start the app as usual...
      Application.Initialize;
      Application.CreateForm(TForm1, Form1);
  Application.Run;

    end
  else
    begin

      //No, so send the param strings to the first instance...
      SingleInstance.SendParamStrings;

    end;


end.
