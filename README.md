# Delphi Single Instance Framework

This repo contains a cross-platform single instance checking framework for Delphi.

This source code is copyright Angelic Technology.

You may use this code in your projects free of charge. While no credit is needed it is appreciated.