// ******************************************************************
//
// Program Name   : SIDemoVCL
// Platform(s)    : Win32, Win64
// Framework      : VCL
//
// Filename       : SIDemoVCL.dpr
// File Version   : 1.00
// Date Created   : 08-Dec-2017
// Author         : Matthew Vesperman
//
// Description:
//
// Sample VCL app to demonstrate the flexible, cross-platform single
// instance checking framework.
//
// Revision History:
//
// v1.00   :   Initial version
//
// ******************************************************************
//
// COPYRIGHT � 2017 - PRESENT Angelic Technology
// ALL RIGHTS RESERVED WORLDWIDE
//
// ******************************************************************

/// <summary>
///   Sample VCL app to demonstrate the flexible, cross-platform
///   single instance checking framework.
/// </summary>
program SIDemoVCL;

uses
  Vcl.Forms,
  Vcl.Dialogs,
  AT.SIDemo.VCL.Forms.Main in 'Forms\AT.SIDemo.VCL.Forms.Main.pas' {Form2},
  AT.SingleInstance.Base in 'Code Units\AT.SingleInstance.Base.pas',
  AT.SingleInstance in 'Code Units\AT.SingleInstance.pas',
  AT.SingleInstance.Windows in 'Code Units\AT.SingleInstance.Windows.pas',
  Vcl.Themes,
  Vcl.Styles;

{$R *.res}

var
  ASuccess: Boolean;  //Indicates if the check was successful...
  AIsFirst: Boolean;  //Flag to indicate first instance state...
  AError: Integer;    //Holds any error code...

begin
  //Check if we are the first instance...
  ASuccess := TATSingleInstance.CheckSingleInstance(
      '{66D1F0EB-7380-409D-AF04-A94E88828675}', AIsFirst, AError);

  //Was check successful???
  if (NOT ASuccess) then
    begin
      //No, display an error message...
      ShowMessageFmt('Single instance check failed! (Error: %d)',
          [AError]);

      //And terminate the app...
      Halt(AError);
    end;

  //Is this the first instance???
  if (AIsFirst) then
    begin
      //Yes, so start the listener to wait for param strings...
      SingleInstance.StartListening;

      //And start the app as usual...
      Application.Initialize;
      Application.MainFormOnTaskbar := True;
      TStyleManager.TrySetStyle('Windows10');
      Application.CreateForm(TForm2, Form2);
  Application.Run;

    end
  else
    begin

      //No, so send the param strings to the first instance...
      SingleInstance.SendParamStrings;

    end;

end.
